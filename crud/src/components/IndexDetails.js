import { useEffect } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { set_localstorage} from "../reduxcomponent/action/action";
import { routerallfunction } from "../Router/router";
function IndexDetails(props) {

  useEffect(()=>{
   const cacheStorage =JSON.parse(localStorage.getItem("userdetail"))|| [];
   props.updatedetails(cacheStorage);
  },[props])

  return (
    <div className="mainContainer">
      <Switch>
        {routerallfunction.map((item) => (
          <Route
            path={item.path}
            exact={item.exact}
            component={item.components}
          />
        ))}
      </Switch>
    </div>
  );
}
const mapDispacterToProps = (dispatch) => {
  return {
    updatedetails: (val) => dispatch(set_localstorage(val))
  };
};
export default connect(null,mapDispacterToProps) (IndexDetails);
