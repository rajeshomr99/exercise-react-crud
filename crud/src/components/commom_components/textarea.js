function Textarea(props) {
  const { name } = props.data;
  const { label, required } = props.data1;
  const { errormsg1, valuechange, disablestate, changeName } = props;
  return (
    <>
      <div className="title">
        <span className="titlename">{label}</span>
        {required && <span className="symbol">*</span>}
      </div>
      <textarea
        disabled={disablestate}
        className={name + "textarea"}
        value={valuechange}
        name={name}
        onChange={(e) => changeName(e, props.data1.name)}
      ></textarea>
      <div className="errorMsg">{errormsg1}</div>
    </>
  );
}
export default Textarea;
