
function Checkbox(props) {
  const{valuechange}=props;
 // console.log(valuechange);
  return (
    <input
      type="checkbox"
      checked={valuechange}
      onChange={(e) => props.changeName(e, props.data.name)}
    />
  );
}
export default Checkbox;
