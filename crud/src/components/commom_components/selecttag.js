function Select(props) {
  const { changeName, errormsg1, valuechange, optionvalue } = props;
  const { name, required, label } = props.data;
  return (
    <>
      <div className="title">
        <span className="titlename">{label}</span>
        {required && <span className="symbol">*</span>}
      </div>
      <div>
        <select
          name={name}
          value={valuechange}
          onChange={(e) => {
            changeName(e, name);
          }}
        >
          
          <option value={0}>select the {name}</option>
          {optionvalue.map((value) => (
            <option value={value.id}>
              {value.subname || value.name}
            </option>
          ))}
        </select>
      </div>
      <div className="errorMsg">{errormsg1}</div>
    </>
  );
}
export default Select;
