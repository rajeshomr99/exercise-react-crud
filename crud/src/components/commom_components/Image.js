import "../commom_components/style/image.css";
//import { useEffect, } from "react";

function Image(props) {
  // const [imagedetails,setimage]=useState(null);
  //const hiddenFileInput =useRef(null);
  const { label, name, required } = props.data;
  const { reference, errormsg1, valuechange } = props;
  //  console.log(imagedetails);
  console.log(valuechange,name);
  return (
    <>
      <div className="title">
        <span className="titlename">{label}</span>
        {required ? <span className="symbol">*</span> : null}
      </div>
      <div className="imagehead">
        <div className="circle">
          <img src={valuechange} />
        </div>
        <div>
          <input
            type="file"
            onChange={(e) => props.changeName1(e)}
            ref={reference}
            accept="image/png,image/jpeg"
            hidden
          />
        </div>
        <div>
          <input
            type="button"
            onClick={(event) => {
              props.changeName(event, name);
            }}
            className="uploadbutton"
            value="Upload"
          />
        </div>
      </div>
      <div className="errorMsg">{errormsg1}</div>
    </>
  );
}
export default Image;
