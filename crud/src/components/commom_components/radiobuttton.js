function Radiobutton(props) {
  const { name, label, required, Option } = props.data;
  const { changeName, errormsg1, valuechange } = props;
  return (
    <>
      <div className="title">
        <span className="titlename">{label}</span>
        {required && <span className="symbol">*</span>}
      </div>
      <div>
        {Option.map((item) => (
          <>
            <label for="gendercheck">{item.subname}</label>
            <input
              type="radio"
              checked={item.name === valuechange}
              name={name}
              value={item.name}
              onChange={(e) => changeName(e, name)}
            />
          </>
        ))}
      </div>
      <div className="errorMsg">{errormsg1}</div>
    </>
  );
}
export default Radiobutton;
