function Input(props) {
  const { name, label, required, onkey, type } = props.data;
  const { changeName, errormsg1, valuechange } = props;

  return (
    <>
      <div className={name}>
        <div className="title">
          <span className="titlename">{label}</span>
          {required && <span className="symbol">*</span>}
        </div>
      </div>
      <input
        type={type}
        name={name}
        maxLength={props.data.max}
        max={name === "dob" ? props.data.max : null}
        className={name === "pincode" ? "pincode" : null}
        value={valuechange}
        onChange={(e) => changeName(e, name)}
      />
      <div className="errorMsg">{errormsg1}</div>
    </>
  );
}

export default Input;
