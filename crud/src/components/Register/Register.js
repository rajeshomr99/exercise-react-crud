import "./Register.css";
import { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import { intialDetails, schema, schema1 } from "../constant/Variable";
import { add, edit, update } from "../../reduxcomponent/action/action";
import Input from "../commom_components/inputtag";
import Select from "../commom_components/selecttag";
import Textarea from "../commom_components/textarea";
import Checkbox from "../commom_components/checkbox";
import Radiobutton from "../commom_components/radiobuttton";
import Image from "../commom_components/Image";
import sourceimage from "../../asserts/images/download.jpeg";
//import { Update_Index } from "../../reduxcomponent/action/actiontypes";

function Register(props) {
  const [formDetails, setForm] = useState(intialDetails);
  const [errorMessage, setErrorMessage] = useState(intialDetails);
  const [stateOption, setStateOption] = useState([]);
  const hiddenFileInput = useRef(null);

  useEffect(() => {
    if(props.currentindex===-1){
      setForm({...formDetails,image:sourceimage});
    }
    
    if (props.currentindex > -1) {
      let stateoptios =
        schema
          .find((sc) => sc.name === "country")
          ?.Option?.find(
            (optio) =>
              optio.id === parseInt(props.studentDetails[props.currentindex].country)
          )?.data || [];
      setStateOption(stateoptios);
      setForm({ ...props.studentDetails[props.currentindex] })
    }
  }, []);
  //console.log(formDetails);
  const handleChange1 = (event) => {
    const fileUploaded = event.target.files[0];
    if(fileUploaded !== "undefined"){
      setForm({ ...formDetails, image: URL.createObjectURL(fileUploaded) });
    } 
    };
  //  console.log(formDetails)
  const handleChange = (event, name) => {
    //  console.log(name,event.target.value);
    setForm({ ...formDetails, [name]:name==="image"?formDetails[name]:event.target.value });

    if (name === "image") {
      if(sourceimage===formDetails[name]){
        setForm({ ...formDetails, [name]: sourceimage });
      }
      
      hiddenFileInput.current.click();
    }
    setErrorMessage({ ...errorMessage, [name]: "" });
    if (name === "communicationaddress") {
      if (formDetails.sameAddress) {
        setForm({
          ...formDetails,
          [name]: event.target.value,
          permanentaddress: event.target.value,
        });
      }
    }

    if (name === "checkbox") {
      let temp = formDetails.sameAddress === true ? false : true;
      //console.log(temp);
      if(temp){
        setForm({
          ...formDetails,
          permanentaddress: formDetails.communicationaddress,sameAddress:temp});
          setErrorMessage({...errorMessage,permanentaddress:""})    
      }
      
      else{
        setForm({...formDetails,sameAddress:temp})
      }
      
    }
    if (name === "country") {
      let stateoptions =
        schema
          .find((sc) => sc.name === name)
          ?.Option?.find((optio) => optio.id ===parseInt( event.target.value))?.data || [];
      setStateOption(stateoptions);
    }
  };
  const validatinform = (name, value, errorMessagec) => {
    if (name === "firstname" || name === "lastname" || name === "city") {
      if (!value.match(/^[a-zA-Z]+$/)) {
        return true;
      }
    }
    if (name === "mobile_no") {
      if (!value.match(/^[1-9]{1}[0-9]{9}$/)) {
        return true;
      }
    }
    if(name==="image"){
      return value === sourceimage?true:false;
    }
    if(name==="communicationaddress"|| name==="permanentaddress"){
      if(!value.trim()){
        setForm({...formDetails,[name]:""})
        return true;
      }
    }
    if (name === "email") {
      if (!value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
        // setErrorMessage({ ...errorMessage, email: `email is already exits` });
        return true;
      }
      // eslint-disable-next-line array-callback-return
      let checkEmail = props.studentDetails.some((user, i) => {

        if (i !== props.currentindex) {
          return user.email === value;
        }
      });

      if (checkEmail) {
        //setErrorMessage({ ...errorMessage, email: `email is already exits` });
        return true;
      }

    }
    if (name === "pincode") {
      if (!value.match(/^[1-9]{1}[0-9]{5}$/)) {
        return true;
      }
    }
    if (name === "organizationname" || name === "state" || name === "country") {
      if (value === "0") {
        return true;
      }
    }
    return false;
  };
  function submitForm(event) {
    event.preventDefault();
    let check = true;

    let errorMessageclone = { ...errorMessage };

    for (const i in formDetails) {
      if ((!formDetails[i] && i !== "sameAddress") || validatinform(i, formDetails[i])) {
        check = false;
       // console.log(i)
        errorMessageclone[i] = `enter the valid ${i}`;
        if (i === "email") {
          // eslint-disable-next-line array-callback-return
          let checkEmail = props.studentDetails.some((user, index) => {
            if (index !== props.currentindex) {
              return user.email === formDetails[i];
            }
          });
    
          if (checkEmail) {
            errorMessageclone[i]='email is already exits';
          }        
        }
      }
    }
    setErrorMessage({ ...errorMessageclone });
    //console.log(check);
    if (check) {
      //debugger;
      if (props.currentindex > -1) {
        props.updatedetails(formDetails);
      } else {
        props.additem(formDetails);
      }
      setForm(intialDetails);

      setErrorMessage(intialDetails);
      props.history.push("/");
    }
  }
  const resetForm = () => {
    if (props.currentindex > -1) {
      props.updateIndexDetails(-1);
    }
    setForm(intialDetails);
    setErrorMessage(intialDetails);
    props.history.push("/");
  };

  return (
    <div className="register">
      <form
        onSubmit={(event) => {
          submitForm(event);
        }}
      >
        <div className="section1">
          {schema.map((item) => (
            <div className={item.name}>
              {item.name === "image" && (
                <Image
                  data={item}
                  errormsg1={errorMessage[item.name]}
                  valuechange={formDetails[item.name]}
                  reference={hiddenFileInput}
                  changeName1={handleChange1}
                  changeName={handleChange}
                />
              )}
              {item.field === "input" && item.name !== "gender" && (
                <Input
                  data={item}
                  errormsg1={errorMessage[item.name]}
                  valuechange={formDetails[item.name]}
                  changeName={handleChange}
                />
              )}
              {item.name === "gender" && (
                <Radiobutton
                  data={item}
                  errormsg1={errorMessage[item.name]}
                  valuechange={formDetails[item.name]}
                  changeName={handleChange}
                />
              )}
              {item.field === "select" && (
                <Select
                  data={item}
                  optionvalue={
                    item.name === "state" ? stateOption : item.Option
                  }
                  errormsg1={errorMessage[item.name]}
                  valuechange={formDetails[item.name]}
                  changeName={handleChange}
                />
              )}
            </div>
          ))}
        </div>
        <div className="section2">
          {schema1.map((item) => (
            <div className={item.name}>
              {item.name === "address" &&
                item.Option.map((i) => (
                  <div className={i.name}>
                    <Textarea
                      data={item}
                      disablestate={
                        i.name === "permanentaddress" ?formDetails.sameAddress : false
                      }
                      errormsg1={errorMessage[i.name]}
                      valuechange={formDetails[i.name]}
                      data1={i}
                      changeName={handleChange}
                    />
                  </div>
                ))}
              {item.name === "checkbox" && (
                <div>
                  <Checkbox
                    data={item}
                    valuechange={formDetails.sameAddress}
                    changeName={handleChange}
                  />
                  <span>Permanent Address same as Communication Address</span>
                </div>
              )}
              {item.name === "pincode" && (
                <div className={item.name}>
                  <Input
                    data={item}
                    errormsg1={errorMessage[item.name]}
                    valuechange={formDetails[item.name]}
                    changeName={handleChange}
                  />
                </div>
              )}
              {item.name === "submitbutton" && (
                <div className="submitButton">
                  <input
                    type="button"
                    onClick={resetForm}
                    className="cancel"
                    value="Cancel"
                  />
                  <input
                    type="submit"
                    className="submit"
                    value={props.currentindex > -1 ? "Update" : "Register"}
                  />
                </div>
              )}
            </div>
          ))}
        </div>
      </form>
    </div>
  );
}
const mapStateToPrpops = (state) => {
  return state;
};
const mapDispacterToProps = (dispatch) => {
  return {
    additem: (val) => dispatch(add(val)),
    updatedetails: (val) => dispatch(update(val)),
    updateIndexDetails: (val) => dispatch(edit(val)),
  };
};
export default connect(mapStateToPrpops, mapDispacterToProps)(Register);
