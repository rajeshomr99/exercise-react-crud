import "./Home.css";
import { tableconten } from "../constant/Variable";
import { connect } from "react-redux";
import { delet, edit } from "../../reduxcomponent/action/action";
function Home(props) {
  const changecomponent = () => {
    props.history.push("/register");
  };
  function editdetails(id) {
    props.edititem(id);
    props.history.push("/register");
  }

  return (
    <div>
      <div className="create">
        <button onClick={changecomponent}>create</button>
      </div>
      <div className="tableContent">
        <table>
          <thead>
            <tr>
              {tableconten.map((item) => (
                <th>{item}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {props.studentDetails.map((item, id) => (
              <tr>
                {tableconten.map((itemvalue) => {
                  switch (itemvalue) {
                    case "S.No":
                      return <td>{id + 1}</td>;
                    case "Name":
                      return <td>{item.firstname + " " + item.lastname}</td>;
                    case "Gender":
                      return <td>{item.gender}</td>;
                    case "DOB":
                      return <td>{item.dob}</td>;
                    case "Mobile Number":
                      return <td>{item.mobile_no}</td>;
                    case "Email Id":
                      return <td>{item.email}</td>;
                    case "Edit/Delete":
                      return (
                        <td>
                          <span
                            className="editmethod"
                            onClick={() => editdetails(id)}
                          >
                            Edit
                          </span>
                          <span className="space"> /</span>
                          <span
                            className="deletemethod"
                            onClick={() => props.deletitem(id)}
                          >
                            Delete
                          </span>
                        </td>
                      );

                    default:
                      break;
                  }
                })}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
const mapStateToPrpops = (state) => {
  return state;
};
const mapDispacterToProps = (dispatch) => {
  return {
    edititem: (val) => dispatch(edit(val)),
    deletitem: (val) => dispatch(delet(val)),
  };
};
export default connect(mapStateToPrpops, mapDispacterToProps)(Home);
