import * as router from "./routerpath";
import Home from "../components/Home/Home";
import Register from "../components/Register/Register";
export const routerallfunction = [
  {
    path: router.home,
    exact: true,
    components: Home,
  },
  {
    path: router.register,
    exact: false,
    components: Register,
  },
];
