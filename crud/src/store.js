import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import crudOperation from "./reduxcomponent/reducers/curdOperationreducer";
const store = createStore(crudOperation, applyMiddleware(thunk));
store.subscribe(()=>{
const {studentDetails}=store.getState();
localStorage.setItem("userdetail", JSON.stringify(studentDetails))
});

export default store;
