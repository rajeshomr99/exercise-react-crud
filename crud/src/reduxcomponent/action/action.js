import { Update_Details, Update_Index } from "./actiontypes";
export const add = (val) => {
  return (dispatch, getState) => {
    let { studentDetails } = getState();
    studentDetails = [...studentDetails, val];
    dispatch({ type: Update_Details, payload: studentDetails });
  };
};
export const edit = (val) => {
  return (dispatch, getState) => {
    dispatch({ type: Update_Index, payload: val });
  };
};
export const delet = (val) => {
  return (dispatch, getState) => {
    let { studentDetails } = getState();
    studentDetails.splice(val, 1);
    dispatch({ type: Update_Details, payload: studentDetails });
  };
};
export const update = (val) => {
  return (dispatch, getState) => {
    let { studentDetails, currentindex } = getState();
    studentDetails.splice(currentindex, 1, val);
    dispatch({ type: Update_Details, payload: studentDetails });
  };
};  
export const set_localstorage=(val)=>{
  return (dispatch, getState) => {
    dispatch({ type: Update_Details, payload: val });
  }; 
}
