import { Update_Details, Update_Index } from "../action/actiontypes";
const initialstate = {
  studentDetails: [],
  currentindex: -1, 
};
const crudOperation = (state = initialstate, action) => {
  switch (action.type) {

    case Update_Details:
      return {
        ...state,
        studentDetails: [...action.payload],
        currentindex: -1, 
      };
    case Update_Index:
      return { ...state, currentindex: action.payload };
    default:
      return state;
  }
};
export default crudOperation;
